﻿namespace Vasos_FMG.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            LoadApplication(new Vasos_FMG.App());
        }
    }
}
